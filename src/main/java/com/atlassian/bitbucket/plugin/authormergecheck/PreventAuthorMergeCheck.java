package com.atlassian.bitbucket.plugin.authormergecheck;

import com.atlassian.bitbucket.hook.repository.RepositoryMergeRequestCheck;
import com.atlassian.bitbucket.hook.repository.RepositoryMergeRequestCheckContext;
import com.atlassian.bitbucket.i18n.I18nService;
import com.atlassian.bitbucket.pull.PullRequest;
import com.atlassian.bitbucket.pull.PullRequestRef;
import com.atlassian.bitbucket.scm.pull.MergeRequest;
import com.atlassian.bitbucket.user.ApplicationUser;
import com.atlassian.bitbucket.auth.AuthenticationContext;

import javax.annotation.Nonnull;

public class PreventAuthorMergeCheck implements RepositoryMergeRequestCheck {

    private final AuthenticationContext authenticationContext;
    private final I18nService i18nService;

    public PreventAuthorMergeCheck(AuthenticationContext authenticationContext, I18nService i18nService) {
        this.authenticationContext = authenticationContext;
        this.i18nService = i18nService;
    }

    @Override
    public void check(@Nonnull RepositoryMergeRequestCheckContext context) {
        ApplicationUser currentUser = authenticationContext.getCurrentUser();
        MergeRequest mergeRequest = context.getMergeRequest();
        if (currentUser != null && isAuthor(currentUser, mergeRequest.getPullRequest())) {
            preventMerge(mergeRequest);
        }
    }

    private void preventMerge(MergeRequest mergeRequest) {
        mergeRequest.veto(
                i18nService.getText("stash.plugin.prevent.author.merging.summary", "You cannot merge your own pull requests"),
                i18nService.getText("stash.plugin.prevent.author.merging.details", "Authors are not allowed to merge their own pull requests. " +
                        "You will need to ask another contributor with write access " +
                        "to {0} to merge it instead.",
                        mergeRequest.getPullRequest().getToRef().getDisplayId())
        );
    }

    private static boolean isAuthor(ApplicationUser user, PullRequest pullRequest) {
        return user.equals(pullRequest.getAuthor().getUser());
    }

}
